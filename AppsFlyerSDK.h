//
//  AppsFlyerSDK.h
//  HRGSDKLibDemo
//
//  Created by hrg on 2018/9/25.
//  Copyright © 2018年 HRG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppsFlyerLib/AppsFlyerLib.h>

@interface AppsFlyerSDK : NSObject

//AppsFlyer初始化
+ (void)initWithAppId:(NSString *)appId devKey:(NSString *)devKey isDebug:(BOOL)isDebug;

//追踪应用启动
+ (void)startTrack;

//追踪创角
+ (void)trackCreateUser:(NSString *)userId;

//追踪升级
+ (void)trackLevelUp:(NSString *)userId level:(int)level;

//追踪完成章节
+ (void)trackReachChapter:(NSString *)userId chapter:(int)chapter;

//追踪初次登录
+ (void)trackFirstLogin:(NSString *)userId;

//追踪登录
+ (void)trackLogin:(NSString *)userId;

//追踪应用内支付成功
+ (void)trackPurchase:(NSString *)orderId goodsName:(NSString *)goodsName currency:(NSString *)currency price:(NSString *)price;

//获取AppsFlyer版本
+ (NSString *)getAppsFlyerVersion;

//追踪忠实用户
+ (void)trackLoyalUser:(NSString *)userId level:(int)level;

//获取AppsFlyer设备ID
+ (NSString *)getAppsFlyerDeviceID;

//
+ (void)trackSelfDefEvent:(NSString *)event userid:(NSString *)userid;
@end
