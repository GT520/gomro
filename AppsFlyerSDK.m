//
//  AppsFlyerSDK.m
//  HRGSDKLibDemo
//
//  Created by hrg on 2018/9/25.
//  Copyright © 2018年 HRG. All rights reserved.
//

#import "AppsFlyerSDK.h"


#define TAG @"AppsFlyerSDK"

#define CREATE_USER @"create_user"
#define FIRST_LOGIN @"first_login"
#define LOGIN @"login"
#define LEVEL_UP @"LV"
#define REACH_CHAPTER @"Chapter"
#define LOYAL_USER @"af_lv10"

@implementation AppsFlyerSDK

+ (void)initWithAppId:(NSString *)appId devKey:(NSString *)devKey isDebug:(BOOL)isDebug{
    [AppsFlyerLib shared].appleAppID = appId;
    [AppsFlyerLib shared].appsFlyerDevKey = devKey;
    [AppsFlyerLib shared].isDebug = isDebug;
  
}

+ (void)startTrack {
    [[AppsFlyerLib shared] start];

}

+ (void)trackCreateUser:(NSString *)userId {
    [[AppsFlyerLib shared] logEvent:CREATE_USER withValues:@{AFEventParamCustomerUserId:userId}];
 
}

+ (void)trackLevelUp:(NSString *)userId level:(int)level {
    if (level >= 10 && level <= 100 && (level % 5 == 0) && (level % 10 == 0 || level > 40)) {
        NSString *levelStr = [NSString stringWithFormat:@"%d",level];
        [[AppsFlyerLib shared] logEvent:[LEVEL_UP stringByAppendingString:levelStr] withValues:@{AFEventParamCustomerUserId:userId}];
      
    } else {
  
    }
}

+ (void)trackReachChapter:(NSString *)userId chapter:(int)chapter {
    if (chapter >= 2 && chapter <= 19) {
        NSString *chapterStr = [NSString stringWithFormat:@"%d",chapter];
        [[AppsFlyerLib shared] logEvent:[REACH_CHAPTER stringByAppendingString:chapterStr] withValues:@{AFEventParamCustomerUserId:userId}];
      
    } else {
    
    }
}

+ (void)trackFirstLogin:(NSString *)userId {
    [[AppsFlyerLib shared] logEvent:FIRST_LOGIN withValues:@{AFEventParamCustomerUserId:userId}];

}

+ (void)trackLogin:(NSString *)userId {
    [[AppsFlyerLib shared] logEvent:AFEventLogin withValues:@{AFEventParamCustomerUserId:userId}];
    [[AppsFlyerLib shared] logEvent:LOGIN withValues:@{AFEventParamCustomerUserId:userId}];

}

+ (void)trackPurchase:(NSString *)orderId goodsName:(NSString *)goodsName currency:(NSString *)currency price:(NSString *)price {
    [[AppsFlyerLib shared] logEvent:AFEventPurchase withValues: @{AFEventParamContentId:orderId, AFEventParamContentType:goodsName, AFEventParamRevenue:price, AFEventParamCurrency:currency, AFEventParamPrice:price}];
  
}

+ (NSString *)getAppsFlyerVersion {
    return [[AppsFlyerLib shared] getSDKVersion];
}

+ (void)trackLoyalUser:(NSString *)userId level:(int)level {
    if (level != 10) {
        return;
    }
    [[AppsFlyerLib shared] logEvent:LOYAL_USER withValues:@{AFEventParamCustomerUserId:userId}];

}

+ (NSString *)getAppsFlyerDeviceID {
    return [[AppsFlyerLib shared ] getAppsFlyerUID];
}

+ (void)trackSelfDefEvent:(NSString *)event userid:(NSString *)userid {
    [[AppsFlyerLib shared] logEvent:event withValues:@{AFEventParamCustomerUserId:userid}];
}
@end

