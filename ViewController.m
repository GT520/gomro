//
//  ViewController.m
//  demo
// com.hrg.demo



#import "ViewController.h"
#import <AdSupport/AdSupport.h>
//#import "person.h"
//#import "GFQFloatWindow.h"
//#import "HRGAppStorePay.h"
#import <TradPlusAds/MsRewardedVideoAd.h>

//苹果授权登录
#import <AuthenticationServices/AuthenticationServices.h>



typedef void(^successBlock)(NSDictionary* data);
typedef void (^FailureBlock)(NSError *error);

#define KEYCHAIN_IDENTIFIER(a) ([NSString stringWithFormat:@"%@_%@",[[NSBundle mainBundle] bundleIdentifier],a])

@interface ViewController ()<MsRewardedVideoAdDelegate,ASAuthorizationControllerDelegate>

@property (nonatomic, strong) MsRewardedVideoAd *rewardedVideoAd;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rewardedVideoAd = [[MsRewardedVideoAd alloc] init];
    [self.rewardedVideoAd setAdUnitID:@"7E3597F9B7AA64A36487514835AD42E5" isAutoLoad:YES];
    self.rewardedVideoAd.delegate = self;
    
    
    NSLog(@"越狱设备");
//    [self test];
}


/**
 注意点：设置路劲：隐私 》跟踪 》允许app请求跟踪，每次关闭再重新获取到的idfa 都是不同的 ，当关闭跟踪之后，获取的idfa:00000000-0000-0000-0000-000000000000
  因此将新获取的idfa 发给服务器后台 添加上这个idfa，不然 就会无法测试展示视频。
 */
- (IBAction)show:(UIButton *)sender {
    if (self.rewardedVideoAd.isAdReady)
    {
    }
    [self.rewardedVideoAd showAdFromRootViewController:self sceneId:@"0984573B7395F2"];
    
}

- (IBAction)AppleLogin:(UIButton *)sender {
    
    ASAuthorizationAppleIDProvider *provider = [[ASAuthorizationAppleIDProvider alloc] init];
    
    ASAuthorizationAppleIDRequest *request = provider.createRequest;
    
    request.requestedScopes = @[ASAuthorizationScopeEmail, ASAuthorizationScopeFullName];
    
    ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
    
    controller.delegate = self;
    
    [controller performRequests];
    
}

#pragma mark - 越狱
-(BOOL)isJailBreak{
    if ([self openAPPURL] || [self judgeByEv] ||[self judgeByFolderExists] || [self OpenALLAppFolder]) {
        NSLog(@"越狱设备");
        return YES;
    }
    NSLog(@"非越狱设备");
    return  NO;
}
//通过能否打开cydia：//来判断，YES说明可以打开，就是越狱的，NO表示不可以打开
- (BOOL)openAPPURL
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://"]]) {
            NSLog(@"The device is jail broken!");
            return YES;
        }
        NSLog(@"The device is NOT jail broken!");
        return NO;
}
//判定常见的越狱文件
-(BOOL)judgeByFolderExists{
    NSArray *arr = @[@"/Applications/Cydia.app",@"/Library/MobileSubstrate/MobileSubstrate.dylib",@"/bin/bash",@"/usr/sbin/sshd",@"/etc/apt"];
    for (NSString *str  in arr) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@",str]]) {
            NSLog(@"The device is jail broken!");
            return YES;
        }
    }
    NSLog(@"The device is NOT jail broken!");
    return NO;
}
//没有越狱的没有这个权限读取所有应用的名称
-(bool)OpenALLAppFolder{
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/User/Applications/"]) {
        NSLog(@"The device is jail broken!");
       NSArray *arr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:@"/User/Applications/" error:nil];
        NSLog(@"arr=%@",arr);
        return YES;
    }
    NSLog(@"The device is NOT jail broken!");
    return NO;
}
//读取环境变量 这个DYLD_INSERT_LIBRARIES环境变量，在非越狱的机器上应该是空，越狱的机器上基本都会有Library/MobileSubstrate/MobileSubstrate.dylib
-(BOOL)judgeByEv{
    char *env = getenv("DYLD_INSERT_LIBRARIES");
    if (env) {
        NSLog(@"The device is jail broken!");
        return YES;
    }
    NSLog(@"The device is NOT jail broken!");
    return NO;
}

#pragma mark - 文件
-(void)saveCache{
    //应用包所有信息
//    NSLog(@"str = %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]);
    
    NSString *path =  [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, true) firstObject];
//    NSLog(@"path = %@",path);
    
    NSString *filePath = [path stringByAppendingPathComponent:@"Preferences/str.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = NO;
    if (![fileManager fileExistsAtPath:filePath isDirectory:&isDir]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        NSLog(@"不存在");
    }else{
        NSLog(@"存在");
    }
    NSDictionary *dict = @{@"name":@"lnj", @"phone":@"12345678", @"address":@"天朝"};
    [dict writeToFile:filePath atomically:YES];
}
-(void)test{
//        tool *df = [tool shareTool];
//        tool *df1 = [tool shareTool];
//        tool *ds = [[tool alloc]init];
//        tool *ds1 = [[tool alloc]init];
//        NSLog(@"df=%@ df1=%@ ds=%p ds1=%@",df,df1,ds,ds1);
        
        NSLog(@"str = %@",[NSBundle mainBundle].infoDictionary[@"CFBundleName"]);
    
    //注意点：设置路劲：隐私 》跟踪 》允许app请求跟踪，每次关闭再重新获取到的idfa 都是不同的 ，当关闭跟踪之后，获取的idfa:00000000-0000-0000-0000-000000000000
        NSString *IDFA = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
       //72A78773-F251-4797-90EB-BE05F21A33A8    5C8C8031-DD52-4FBC-9D55-12C36B536E3A
        NSLog(@"IDFA = %@",IDFA);
       bool switchON = [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled];
        NSLog(@"switchON=%d",switchON);
        //AA7B771C-B6D8-477D-AB1F-6BC640171EEE
        NSString *IDFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSLog(@"IDFV=%@",IDFV);
}
#pragma mark - 网络
-(void)net {
//        NSString *url = @"https://api.binstd.com/news/get";
//        NSDictionary *dict = @{@"channel":@"头条", @"start":@"0", @"num":@"10", @"appkey":@"9293109afcbff8dd"};
        NSString *url = @"https://api.binstd.com/news/get?channel=头条&start=0&num=10&appkey=9293109afcbff8dd";
        [self getWithUrl:url parameters:nil success:^(NSDictionary *data) {
            NSLog(@"data = %@",data);
        } failure:^(NSError *error) {
            NSLog(@"error = %@",error);
        }];
        

    //    [self postwithURL:url parameters:dict success:^(NSDictionary *data) {
    //        NSLog(@"data = %@",data);
    //    } error:^(NSError *error) {
    //        NSLog(@"error = %@",error);
    //    }];
}
- (void)getWithUrl:(NSString *)url parameters:(NSDictionary *)paramsDict success:(successBlock)successBlock failure:(FailureBlock)failureBlock{
    NSString * urlEnCode = nil;
    if ([paramsDict allKeys]) {
        NSLog(@"-----------------22222-----------------");
        NSMutableString * mutableUrl = [[NSMutableString alloc] initWithString:url];
        [mutableUrl appendString:@"?"];
        for (id key in paramsDict) {
    //此处不需转码（中文）因为外面统一做了转码操作
//            NSString * value = [[paramsDict objectForKey:key] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSString * value = [paramsDict objectForKey:key];
            [mutableUrl appendString:[NSString stringWithFormat:@"%@=%@&", key, value]];
        }
        //统一转码
        // 删除mutableUrl最后一位&  [mutableUrl substringToIndex:mutableUrl.length - 1]
       urlEnCode = [[mutableUrl substringToIndex:mutableUrl.length - 1]  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }else{
        NSLog(@"-----------------11111------------------");
        urlEnCode = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    NSLog(@"URL编码=%@",urlEnCode);
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlEnCode] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5];
    NSURLSession * urlSession = [NSURLSession sharedSession];
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            failureBlock(error);
        }else{
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            successBlock(dict);
        }
    }];
    [dataTask resume];
}

-(void)postwithURL:(NSString *)URL parameters:(NSDictionary*)parameters success:(successBlock)successBlock failure:(FailureBlock)failureBlack{
    NSMutableString *str = [[NSMutableString alloc]init];
    for (id key in parameters) {
        NSString *value = [parameters objectForKey:key];
        [str appendFormat:@"%@=%@&",key,value];
    }
    NSString *strEncode = [[str substringToIndex:str.length -1] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [strEncode dataUsingEncoding:NSUTF8StringEncoding];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *sesionTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            failureBlack(error);
        }else{
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            successBlock(dict);
        }
    }];
    [sesionTask resume];
}

#pragma mark - MsRewardedVideoAdDelegate implementation
- (void)rewardedVideoAdAllLoaded:(MsRewardedVideoAd *)rewardedVideoAd readyCount:(int)readyCount
{
    NSLog(@"%s->ready:%d", __FUNCTION__, self.rewardedVideoAd.readyAdCount);
//        dispatch_async(dispatch_get_main_queue(), ^{
//    //    if (rewardedVideoAd.readyAdCount > 0)
//    //        self.btnShow.enabled = YES;
//        self.btnLoad.enabled = YES;
//        [self.activityIndicatorView stopAnimating];
//        self.textView.text = [self.rewardedVideoAd getLoadDetailInfo];
//        });
}

- (void)rewardedVideoAdLoaded:(MsRewardedVideoAd *)rewardedVideoAd
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)rewardedVideoAd:(MsRewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error
{
    NSLog(@"%s->%@", __FUNCTION__, error);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self.btnLoad.enabled = YES;
//        [self.activityIndicatorView stopAnimating];
//    });
}

- (void)rewardedVideoAdShown:(MsRewardedVideoAd *)rewardedVideoAd
{
    NSLog(@"%s", __FUNCTION__);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self->_lblCacheNum.text = rewardedVideoAd.channelName;
//    });
}

- (void)rewardedVideoAdClicked:(MsRewardedVideoAd *)rewardedVideoAd
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)rewardedVideoAdShouldReward:(MsRewardedVideoAd *)rewardedVideoAd reward:(MSRewardedVideoReward *)reward
{
    NSLog(@"%s", __FUNCTION__);
//    dispatch_async(dispatch_get_main_queue(), ^{
//    if (reward)
//    {
//        NSString *strReward = [NSString stringWithFormat:@"reward type:%@ amount:%d", reward.currencyType, [reward.amount intValue]];
//        self->_lblRewardInfo.text = strReward;
//    }
//    });
}

- (void)rewardedVideoAdShouldNotReward:(MsRewardedVideoAd *)rewardedVideoAd
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)rewardedVideoAdDismissed:(MsRewardedVideoAd *)rewardedVideoAd
{
    NSLog(@"%s", __FUNCTION__);
    //如果setAdUnitID 参数isAutoLoad 为NO 需调用
    //[self.rewardedVideoAd loadAd];
}

- (void)loadingInfoChangedR:(MsRewardedVideoAd *)rewardedVideoAd
{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self.textView.text = [self.rewardedVideoAd getLoadDetailInfo];
//        self.textViewStatus.text = [self.rewardedVideoAd getLoadDetailStatus];
////        _btnShow.enabled = self.rewardedVideoAd.readyAdCount > 0;
////        _lblCacheNum.text = [NSString stringWithFormat:@"%d", self.rewardedVideoAd.cacheNum];
//    });
}




- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization{
    NSLog(@"Sign With Apple did complate with authorization");
    
    ASAuthorizationAppleIDCredential *credential = authorization.credential;
    NSString *userID = credential.user;
    NSString *email = credential.email;
    NSPersonNameComponents *fullName = credential.fullName;
    NSString *state = credential.state;
    ASUserDetectionStatus realUerStatus = credential.realUserStatus;
    NSString *identityToken = [credential.identityToken base64Encoding];
    NSString *authorizationCode = [credential.authorizationCode base64Encoding];
    
    NSLog(@"Sign With Apple - userID: %@", userID);
    NSLog(@"Sign With Apple - email: %@", email);
    NSLog(@"Sign With Apple - fullName: %@", fullName);
    NSLog(@"Sign With Apple - state: %@", state);
    NSLog(@"Sign With Apple - realUerStatus: %ld", (long)realUerStatus);
    NSLog(@"Sign With Apple - identityToken: %@", identityToken);
    NSLog(@"Sign With Apple - authorizationCode: %@", authorizationCode);
}
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error{
    NSLog(@"Sign With Apple did complate with error: %@", error);
}
@end

